# README #

It's a java project.

### What is this repository for? ###

* Repository for WTM GDG challenge
* Version 0.0.1

### How do I get set up? ###

* Install Java 8
* Install Maven
* Open any IDE that supports Java 8 and Spring Framework and import this project as Maven project
* Run `mvn clean install` to install the dependencies
* Run `mvn run` to run the project