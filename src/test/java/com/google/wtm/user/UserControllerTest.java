package com.google.wtm.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.wtm.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@WebAppConfiguration
public class UserControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testCreateUser() throws Exception {
		User user = new User();
		user.setUserId(1L);
		user.setEmail("adheli.dev@gmail.com");
		user.setName("Adheli Tavares");

		String raw = new Gson().toJson(user);

		this.mockMvc.perform(MockMvcRequestBuilders.post("/rest/user/save").contentType(CONTENT_TYPE).content(raw))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
				.andExpect(MockMvcResultMatchers.jsonPath("$.userId").isNotEmpty())
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value(user.getName()));
	}
	
	@Test
	public void testUserById() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/rest/user/userById?id=1"))
		.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
		.andExpect(MockMvcResultMatchers.jsonPath("$.userId").exists());
	}

}
