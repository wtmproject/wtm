package com.google.wtm.loan;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.wtm.model.Loan;
import com.google.wtm.model.PaymentStatus;
import com.google.wtm.model.PaymentType;
import com.google.wtm.model.User;
import com.google.wtm.service.LoanService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
@WebAppConfiguration
public class LoanControllerTest {

	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	private LoanService loanService;

	private MockMvc mockMvc;

	private Loan loan;

	private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

	@Before
	public void setUp() throws Exception {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		User user = new User();
		user.setUserId(1L);

		loan = new Loan();
		loan.setId(1L);
		loan.setAmount(10.0);
		loan.setComment("Emprestei mesmo");
		loan.setPaymentType(PaymentType.CARD);
		loan.setStatus(PaymentStatus.NEW);
//		loan.setDate(new Date());
		loan.setDebtor(user);
		loan.setCollector(user);

	}

	@Test
	public void testCreateLoan() throws Exception {

		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

		String raw = new Gson().toJson(loan);
		
		System.out.println(raw);

		this.mockMvc.perform(MockMvcRequestBuilders.post("/rest/loan/new").contentType(CONTENT_TYPE).content(raw))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
	}

	/*@After
	public void tearDown() {
		this.loanService.removeLoan(loan);
	}
*/
}
