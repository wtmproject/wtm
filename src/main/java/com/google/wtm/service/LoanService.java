package com.google.wtm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.wtm.model.Bank;
import com.google.wtm.model.Loan;
import com.google.wtm.repository.LoanRepository;

@Service
public class LoanService {
	
	@Autowired
	private LoanRepository loanRepository;
	
	public void removeLoan(Loan loan) {
		this.loanRepository.delete(loan);
		
	}
	
	public Loan createLoan(Loan loan) {
		return this.loanRepository.saveAndFlush(loan);
	}
	
	
	
	

}
