package com.google.wtm.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.wtm.model.Account;
import com.google.wtm.model.User;
import com.google.wtm.repository.AccountRepository;
import com.google.wtm.repository.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository;
	private AccountRepository accountRepository;

	@Autowired
	public UserService(UserRepository userRepository, AccountRepository accountRepository) {
		this.userRepository = userRepository;
		this.accountRepository = accountRepository;
	}

	public User saveUser(User user) {
		return this.userRepository.saveAndFlush(user);
	}

	public User findUser(Long id) {
		try {
			User user = this.userRepository.findById(id).get();
			System.out.println(user.getEmail());
			return user;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public User findUserByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	public List<User> allUsers() {
		return this.userRepository.findAll();
	}
	
	public User createAccount(Account account) {
		Account newAccount = this.accountRepository.saveAndFlush(account);
		return newAccount.getUser();
	}
}
