package com.google.wtm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.wtm.model.Bank;
import com.google.wtm.repository.BankRepository;

@Service
public class BankService {

	private BankRepository bankRepository;
	
	@Autowired
	public BankService(BankRepository bankRepository) {
		this.bankRepository = bankRepository;
	}
	
	public Bank createBank(Bank bank) {
		return this.bankRepository.saveAndFlush(bank);
	}
	
	public List<Bank> getBanks() {
		return this.bankRepository.findAll();
	}
	
}
