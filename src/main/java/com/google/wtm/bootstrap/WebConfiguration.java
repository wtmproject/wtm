package com.google.wtm.bootstrap;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class WebConfiguration {

	/**
	 * method so the Allow Headers are enabled in the web browser when calling
	 * local rest services
	 * 
	 * @return CorsFilter configured for allowing localhost requests from the
	 *         aforementioned methods.
	 */
	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");

		config.setAllowedHeaders(Arrays.asList("*"));
		
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}
