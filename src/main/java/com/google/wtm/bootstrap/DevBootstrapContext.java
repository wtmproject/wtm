package com.google.wtm.bootstrap;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.google.wtm.model.Bank;
import com.google.wtm.service.BankService;

@Component
public class DevBootstrapContext implements ApplicationListener<ContextRefreshedEvent> {

	private BankService bankService;
	private ResourceLoader resourceLoader;
	
	private static final String jsonConfigFolder = "classpath:init/";
	
	@Autowired
	public DevBootstrapContext(BankService bankService, ResourceLoader resourceLoader) {
		this.bankService = bankService;
		this.resourceLoader = resourceLoader;
	}
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		try {
			this.initBankData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initBankData() throws IOException {
		Gson jsonMapper = new GsonBuilder().setPrettyPrinting().create();
		Resource resource = resourceLoader.getResource(jsonConfigFolder + "banks.json");
		Reader reader = new InputStreamReader(resource.getInputStream());

		Type jsonBanks = new TypeToken<List<Bank>>() {}.getType();
		
		List<Bank> objectBanks = jsonMapper.fromJson(reader, jsonBanks);

		if (!objectBanks.isEmpty()) {
			objectBanks.forEach(bankService::createBank);
		}
	}
}
