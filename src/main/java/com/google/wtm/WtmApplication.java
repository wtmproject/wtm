package com.google.wtm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WtmApplication {

	public static void main(String[] args) {
		SpringApplication.run(WtmApplication.class, args);
	}
}
