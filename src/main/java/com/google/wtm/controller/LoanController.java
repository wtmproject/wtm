package com.google.wtm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.wtm.model.Loan;
import com.google.wtm.model.User;
import com.google.wtm.service.LoanService;
import com.google.wtm.service.UserService;

@RestController
@RequestMapping("/rest/loan/")
public class LoanController {

	private LoanService loanService;

	@Autowired
	public LoanController(LoanService loanService) {
		this.loanService = loanService;
	}

	@RequestMapping(path = "new", method = RequestMethod.POST)
	public ResponseEntity<Loan> createLoan(@RequestBody Loan loan) {

		Loan savedLoan = this.loanService.createLoan(loan);
		return new ResponseEntity<Loan>(savedLoan, HttpStatus.CREATED);
	}

	@RequestMapping(path = "update", method = RequestMethod.PUT)
	public ResponseEntity<Loan> updateLoan(@RequestBody Loan loan) {
		return new ResponseEntity<Loan>(HttpStatus.OK);
	}

	@RequestMapping(path = "loansByDebtor", method = RequestMethod.POST)
	public ResponseEntity<List<Loan>> getLoansByDebtor(@RequestParam Long userId) {
		return new ResponseEntity<List<Loan>>(HttpStatus.OK);
	}

	@RequestMapping(path = "loansByCollector", method = RequestMethod.POST)
	public ResponseEntity<List<Loan>> getLoansByCollector(@RequestParam Long userId) {
		return new ResponseEntity<List<Loan>>(HttpStatus.OK);
	}

	@RequestMapping(path = "confirmPayment")
	public ResponseEntity<Loan> confirmPayment(@RequestParam Long loanId) {
		return new ResponseEntity<Loan>(HttpStatus.OK);
	}
}
