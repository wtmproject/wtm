package com.google.wtm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.wtm.model.Account;
import com.google.wtm.model.User;
import com.google.wtm.service.UserService;

@RestController
@RequestMapping("/rest/user/")
public class UserController {
	
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(path = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> saveUser(@RequestBody User user) {
		User savedUser = this.userService.saveUser(user);
		return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "userByEmail", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<User> getUser(@RequestParam String email) {
		User user = this.userService.findUserByEmail(email);
		
		if (user == null) {			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
	}
	
	@RequestMapping(path = "userById", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<User> getUserById(@RequestParam Long id) {
		User user = this.userService.findUser(id);
		
		if (user == null) {			
			return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
	}
	
	@RequestMapping(path = "all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<User>> getUsers() {
		List<User> users = this.userService.allUsers();
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@RequestMapping(path = "createAccount", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> createAccount(@RequestBody Account account) {
		User user = this.userService.createAccount(account);
		return new ResponseEntity<User>(user, HttpStatus.CREATED);
	}
}
