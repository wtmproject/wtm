package com.google.wtm.model;

public enum PaymentType {

	CARD,
	CASH,
	VR,
	OTHER
}
