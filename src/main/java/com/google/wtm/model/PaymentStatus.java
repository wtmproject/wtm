package com.google.wtm.model;

public enum PaymentStatus {

	NEW,
	PENDING_CONFIRMATION,
	CLOSED
}
